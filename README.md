# API Server Configuration

This project is responsible to fully manage the apiserver.

For now, we only need to configure the public API, adding letsencrypt certificate and
a route for user CLI and remote access by the application
https://gitlab.cern.ch/paas-tools/okd4-install/-/blob/master/docs/design/apiserver-design.md

